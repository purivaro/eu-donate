<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('people', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('center_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('prefix_id')->nullable();
			$table->string('firstname');
			$table->string('lastname')->nullable();
			$table->string('monkname')->nullable();
			$table->string('nickname')->nullable();
			$table->string('gender')->nullable();
			$table->date('dateofbirth')->nullable();
			$table->string('address')->nullable();
			$table->integer('country_id')->nullable();
			$table->string('cover_img')->nullable();
			$table->string('phone')->nullable();
			$table->string('facebook')->nullable();
			$table->string('website')->nullable();
			$table->string('email')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('people');
	}
}
