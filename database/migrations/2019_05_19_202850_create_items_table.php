<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
						$table->integer('center_id')->nullable();
						$table->string('title');
						$table->string('description')->nullable();
						$table->integer('category_id');
						$table->integer('project_id')->nullable();
						$table->integer('type_id')->nullable(); // ประเภทของบุญ เป็นบุญทั่วไป หรือบุญในกิจกรรม
						$table->integer('activity_id')->nullable();
						$table->datetime('start_at');
						$table->datetime('end_at')->nullable();
						$table->string('cover_img')->nullable();
						$table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
