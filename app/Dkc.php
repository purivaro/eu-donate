<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Dkc extends Model
{
  protected $dates = [
    'happened_on'
  ];
  
  protected $fillable = [
    'title',
    'detail',
    'happened_on',
    'user_id',
    'vdo',
  ];

  // protected $guarded = [];



  public function tags()
  {
    return $this->belongsToMany(Tag::class);
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function hasTag($tagId)
  {
    return in_array($tagId, $this->tags->pluck('id')->toArray());
  }

  public function images()
  {
    return $this->hasMany(DkcImage::class);
  }


  public function scopeSearched($query)
  {
    
    $search = request()->query('search');
    // $d = request()->query('d');
    // $m = request()->query('m');
    // $y = request()->query('y');

    if($search){
      $query->where('title', 'LIKE', "%{$search}%")->orWhere('detail', 'LIKE', "%{$search}%");
    }

    if(request()->query('d')){
      // $query->whereRaw("DAY(happened_on) = {$d}");
      $query->whereDay('happened_on', request()->query('d'));
    }

    if(request()->query('m')){
      // $query->whereRaw("MONTH(happened_on) = {$m}");
      $query->whereMonth('happened_on', request()->query('m'));
    }

    if(request()->query('y')){
      // $y = $y-543;
      // $query->whereRaw("YEAR(happened_on) = {$y}");
      $query->whereYear('happened_on', request()->query('y')-543);
    }
    // dd($query);
    return $query;

  }



  public function scopePublished($query)
  {
    return $query->where('happened_on', '<=', now());
  }

  // เพื่อดัดแปลงค่าจากฟิลด์ image ก่อนออกมา ด้วยการเติมชื่อโฟลเดอร์ไปข้างหน้า
  public function getImageAttribute($value)
  {
    return 'storage/'.$value;
  }

}
