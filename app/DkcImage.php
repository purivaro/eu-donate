<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class DkcImage extends Model
{
  protected $fillable = ['dkc_id', 'path'];

  
  // เพื่อดัดแปลงค่าจากฟิลด์ path ก่อนออกมา ด้วยการเติมชื่อโฟลเดอร์ไปข้างหน้า
  public function getPathAttribute($value)
  {
    return 'storage/'.$value;
  }

  public function deleteImage()
  {
    return Storage::delete($this->getOriginal('path'));
  }
}
