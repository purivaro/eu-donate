@extends('layouts.main')

@section('title', 'Categories')

@section('error')
  @if(session('error'))
    <div class="alert alert-danger">{{session('error')}}</div>
  @endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header d-flex">
      <div class="mr-auto">Categories</div>
      <a href="{{route('categories.create')}}" class="btn btn-secondary">Add</a>
    </h4>
    <div class="card-body">

      <table class="table mt-2">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Count</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($categories AS $category)
          <tr>
            <td>{{$category->id}}</td>
            <td>{{$category->name}}</td>
            <td>{{$category->activities->count()}}</td>
            <td>
              <a href="{{route('categories.edit', $category->id)}}" class="btn  btn-outline-info">Edit</a>
              <form action="{{route('categories.destroy', $category->id)}}" method="post" class="d-inline-block ml-2">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-outline-danger">Delete</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection