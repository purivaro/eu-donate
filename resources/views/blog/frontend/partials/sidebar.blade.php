<div class="sidebar px-4 py-md-0">

  <h6 class="sidebar-title">Search</h6>
  <form class="" action="" method="GET">
    <div class="form-group input-group">
      <input type="text" class="form-control" name="search" placeholder="Search" value="{{request()->query('search')}}">
      <div class="input-group-addon">
        <span class="input-group-text"><i class="ti-search"></i></span>
      </div>
    </div>
    <div class="form-group row">
      <div class="col d-flex mb-2">
        <select name="d" id="d" class="form-control">
          <option value="">วัน</option>
          @for($i = 1; $i <= 31; $i++)
            <option value="{{$i}}" 
              @if($i==request()->query('d'))
                selected
              @endif
            >{{$i}}</option>
          @endfor
        </select>
        <select name="m" id="m" class="form-control">
          <option value="">เดือน</option>
          @foreach($months as $key=>$month)
            <option value="{{$key}}"
              @if($key==request()->query('m'))
                selected
              @endif
            >{{$month}}</option>
          @endforeach
        </select>
        <select name="y" id="y" class="form-control">
          <option value="">ปี</option>
          @for($i = (Carbon::now()->format('Y')+543); $i >= 2512; $i--)
            <option value="{{$i}}" 
              @if($i==request()->query('y'))
                selected
              @endif
            >{{$i}}</option>
          @endfor
        </select>
      </div>      
    </div>
    <button type="submit" class="btn btn-primary">Search</button>
  </form>

  <hr>

  {{-- <h6 class="sidebar-title">Categories</h6>
  <div class="row link-color-default fs-14 lh-24">
    @foreach($categories AS $category)
      <div class="col-6"><a href="{{route('frontend.category',$category)}}">{{$category->name}}</a></div>
    @endforeach
  </div> --}}


  <h6 class="sidebar-title">Tags</h6>
  <div class="gap-multiline-items-1 mb-5">
    @foreach($tags AS $tag)
      <a class="badge badge-secondary" href="{{route('frontend.tag',$tag)}}">{{$tag->name}}</a>
    @endforeach
  </div>


</div> 