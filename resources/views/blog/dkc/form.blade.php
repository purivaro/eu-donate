@extends('layouts.main')


@section('error')
  @if($errors->any())
    @foreach($errors->all() AS $error)  
      <div class="alert alert-danger">{{$error}}</div>
    @endforeach
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header">{{isset($dkc)?'แก้ไข':'เพิ่ม'}} เหตุการณ์</h4>
    <div class="card-body">
      <form action="{{isset($dkc)?route('dkcs.update', $dkc->id):route('dkcs.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        @if(isset($dkc))
          @method('PUT')
        @endif
        <div class="form-group">
          <label for="happened_on">วันที่</label>
          <div class="d-flex">
            <select name="d" id="d" class="form-control" style="width: 90px;">
              <option value="">วัน</option>
              @for($i = 1; $i <= 31; $i++)
                <option value="{{$i}}" 
                  @if($i==(isset($dkc)?$dkc->happened_on->format('d'):Carbon::now()->format('d')))
                    selected
                  @endif
                >{{$i}}</option>
              @endfor
            </select>
            <select name="m" id="m" class="form-control ml-2" style="width: 90px;">
              <option value="">เดือน</option>
              @foreach($months as $key=>$month)
                <option value="{{$key}}"
                  @if($key==(isset($dkc)?$dkc->happened_on->format('m'):Carbon::now()->format('m')))
                    selected
                  @endif
                >{{$month}}</option>
              @endforeach
            </select>
            <select name="y" id="y" class="form-control ml-2" style="width: 90px;">
              <option value="">ปี</option>
              @for($i = (Carbon::now()->format('Y')+543); $i >= 2512; $i--)
                <option value="{{$i}}" 
                  @if($i==(isset($dkc)?($dkc->happened_on->format('Y')+543):(Carbon::now()->format('Y')+543)))
                    selected
                  @endif
                >{{$i}}</option>
              @endfor
            </select>
          </div>          
          {{-- <input type="text" name="happened_on" id="happened_on" class="form-control" value="{{old('happened_on', isset($dkc)?$dkc->happened_on:Carbon::now())}}"> --}}
        </div>        
        <div class="form-group">
          <label for="title">เหตุการณ์</label>
          <input type="text" name="title" id="title" class="form-control" value="{{old('title', isset($dkc)?$dkc->title:'')}}">
        </div>
        <div class="form-group">
          <label for="detail">รายละเอียด</label>
          <input type="hidden" name="detail" id="detail" class="form-control" value="{{old('detail', isset($dkc)?$dkc->detail:'')}}">
          <trix-editor input="detail"></trix-editor>
        </div>
        <div class="form-group">
          <label for="">รูปภาพ</label>
          @if(isset($dkc))
            <div>
              @foreach($dkc->images AS $image)
                <img src="{{asset($image->path)}}" class="w-25 mr-2 mb-2">
              @endforeach
            </div>
          @endif
          <input type="file" name="images[]" id="images" multiple >
        </div>
        <div class="form-group">
          <label for="vdo">YouTube VDO link</label>
          <input type="text" name="vdo" id="vdo" class="form-control" value="{{old('vdo', isset($dkc)?$dkc->vdo:'')}}">
        </div>
        <div class="form-group">
          <label for="tags">Tags</label>
          <select name="tags[]" id="tags" class="form-control select2" multiple>
            <option value="">-Choose-</option>
            @foreach($tags AS $tag)
              <option value="{{$tag->id}}" 
                @if(isset($dkc))
                  {{$dkc->hasTag($tag->id)?"selected":""}}  
                @endif
              >{{$tag->name}}</option>
            @endforeach
          </select>
        </div>
        <button class="btn btn-primary">บันทึก</button>
        <a href="{{route('dkcs.index')}}" class="btn btn-default ml-2">ยกเลิก</a>
      </form>
    </div>
  </div>
@endsection

@section('css')
  {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.1.0/trix.css"> --}}
  {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css"> --}}

@endsection

@section('scripts')
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.1.0/trix.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script> --}}

  <script>
    document.getElementById('title').focus();

    flatpickr("#happened_on", {
      enableTime: false,
      enableSeconds: false,
      locale: Thai
    })

    $(function(){

      $(".select2").select2();

      // http://plugins.krajee.com/file-input/plugin-options#language
      $("#images").fileinput({
        theme: "fas",
        showUpload:false, 
        showCaption:false, 
        showCancel:false, 
        hideThumbnailContent:false,  
        fileActionSettings: {
          // showZoom:false,
        },
      });



      var element = document.querySelector("trix-editor")
      


      var HOST = "/upload/file"
      addEventListener("trix-attachment-add", function(event) {
          if (event.attachment.file) {
              uploadFileAttachment(event.attachment)
          }
      })
      function uploadFileAttachment(attachment) {
          uploaded = uploadFile(attachment.file, setProgress, setAttributes)
          function setProgress(progress) {
              attachment.setUploadProgress(progress)
          }
          function setAttributes(attributes) {
              // console.log(attachment)
              attachment.setAttributes(attributes)
              
          }
          // console.log(uploaded)
          attachment.remove();
      }
      function uploadFile(file, progressCallback, successCallback) {

          var formData = createFormData(file)
          var xhr = new XMLHttpRequest()
          xhr.open("POST", HOST, true)
          xhr.upload.addEventListener("progress", function(event) {
              var progress = event.loaded / event.total * 100
              progressCallback(progress) 
          })
          xhr.addEventListener("load", function(event) {
              if (xhr.status == 204) {
                  var attributes = {
                      url: HOST,
                      href: HOST + "?content-disposition=attachment"
                  }
                  // console.log(5555)
                  successCallback(attributes)
              }
              // successCallback(`/storage/${event.target.responseText}`)
              current_range = element.editor.getSelectedRange()
              element.editor.setSelectedRange(current_range)
              element.editor.insertHTML(`<img src="/storage/${event.target.responseText}" class='img-fluid'>`)
              
          })
          xhr.send(formData)
          return event.target.responseText
      }

      function createFormData(file) {
          var data = new FormData()
          _token = document.querySelector('[name=_token]').value;
          data.append("_token", _token)
          data.append("file", file)
          data.append("Content-Type", file.type)
          return data
      }




      
    })





    


  </script>
@endsection