@component('mail::message')
# Introduction {{$data['title']}}

The body of your message.

{{$data['content']}}

  @component('mail::button', ['url' => ''])
  Button Text
  @endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
