<nav class="navbar navbar-dark navbar-expand-sm bg-primary mb-3">
  <a href="{{route('dkcs.index')}}" class="navbar-brand">Donate</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navmenu" aria-controls="navmenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navmenu">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        {{-- <li class="nav-item {{ Route::currentRouteName()=='home' ? 'active' : '' }}">
          <a class="nav-link" href="{{route('home')}}">Home</a>
        </li> --}}
        <li class="nav-item {{ Route::currentRouteName()=='activities.index' ? 'active' : '' }} ">
          <a class="nav-link" href="{{route('activities.index')}}">Activity</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName()=='donates.index' ? 'active' : '' }} ">
          <a class="nav-link" href="{{route('donates.index')}}">Donate</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName()=='dkcs.index' ? 'active' : '' }} ">
          <a class="nav-link" href="{{route('dkcs.index')}}">History</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName()=='tags.index' ? 'active' : '' }}">
          <a class="nav-link" href="{{route('tags.index')}}">Tag</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName()=='categories.index' ? 'active' : '' }}">
          <a class="nav-link" href="{{route('categories.index')}}">Category</a>
        </li>
        <li class="nav-item {{ Route::currentRouteName()=='frontend.index' ? 'active' : '' }}">
          <a class="nav-link" href="{{route('frontend.index')}}" target="_blank">ดูหน้าเว็บ</a>
        </li>
        {{-- <li class="nav-item">
          <a class="nav-link" href="{{route('mail.send')}}" target="_blank">Email</a>
        </li> --}}
      </ul>
      <div class="my-2 my-lg-0">
        <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >{{isset(Auth::user()->name)?Auth::user()->name:""}} </button>
          <div class="dropdown-menu dropdown-menu-right">
            <a href="" class="dropdown-item">Profile</a>
            <a href="" class="dropdown-item">Change Password</a>
            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Sign Out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
        </div>
      </div>
    </div>
</nav>
