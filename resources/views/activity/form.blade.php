@extends('layouts.main')

@section('title', 'แบบฟอร์มกิจกรรม')

@section('error')
	@if($errors->any())
		@foreach($errors->all() AS $error)
			<div class="alert alert-danger">{{$error}}</div>
		@endforeach
	@endif
@endsection

@section('status')
  @if(session('status'))
    <div class="alert alert-success">{{session('status')}}</div>
  @endif
@endsection

@section('content')
	<div class="card">
		<h4 class="card-header">แบบฟอร์มกิจกรรม</h4>
		<div class="card-body">
			<form action="{{!isset($activity)? route('activities.store') : route('activities.update', $activity->id)}}" method="post" enctype="multipart/form-data">
				@csrf
				@if(isset($activity))
					@method('put')
				@endif
				<div class="form-group">
					<label for="title">ชื่อกิจกรรม</label>
					<input type="text" name="title" id="title" class="form-control" value="{{old('title', isset($activity)?$activity->title:'')}}">
				</div>
				<div class="form-group">
					<label for="description">รายละเอียด</label>
					<input type="text" name="description" id="description" class="form-control" value="{{old('description', isset($activity)?$activity->description:'')}}">
				</div>
				<div class="form-group">
					<label for="category_id">หมวดหมู่</label>
					<select name="category_id" id="category_id" class="form-control" >
						<option value="">เลือก</option>
						@foreach($categories AS $category)
							<option value="{{$category->id}}"
								@if($category->id==isset($activity)?$activity->category_id:'')
									selected
								@endif
							>{{$category->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="start_at">วันเวลาเริ่ม</label>
					<input type="text" name="start_at" id="start_at" class="form-control" value="{{old('start_at', isset($activity)?$activity->start_at:'')}}">
				</div>
				<div class="form-group">
					<label for="end_at">วันเวลาสิ้นสุด</label>
					<input type="text" name="end_at" id="end_at" class="form-control" value="{{old('end_at', isset($activity)?$activity->end_at:'')}}">
				</div>
				<div class="form-group">
					<label for="cover_img">ภาพประกอบ</label>
					@if(isset($activity) && $activity->getOriginal('cover_img'))
						<img src="{{isset($activity)?asset($activity->cover_img):''}}" class="w-100">
					@endif
					<div>
						<input type="file" name="cover_img" id="cover_img" class="" value="{{old('cover_img')}}">
					</div>
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
@endsection

@section('scripts')
<script>

	$("#cover_img").fileinput({
		theme: "fas",
		showUpload:false,
		showCaption:false,
		showCancel:false,
		hideThumbnailContent:false,
		fileActionSettings: {
			// showZoom:false,
		},
	});

	flatpickr("#start_at", {
		enableTime: true,
		enableSeconds: true
	});

	flatpickr("#end_at", {
		enableTime: true,
		enableSeconds: true
	});

</script>
@endsection